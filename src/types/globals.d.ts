// -------------------------------------------------------------------------
// Расширение существующих пространств имен и интерфейсов.
// -------------------------------------------------------------------------

declare namespace NodeJS {
    interface Global {
        app: App,
        webApi: any,
        logger: import('winston').Logger,
    }
}

// -------------------------------------------------------------------------
// Новые основоположные типы
// -------------------------------------------------------------------------

interface App {
    webServer?: import('http').Server;
    express?: import('express').Express;
    socketServer?: import('socket.io').Server;
}

interface Window {
    router: any,
    webApi: any,
}

interface BotInfo {
    statistics: any[];
    _id: string,
    name: string,

    messages: Messages,

    buttons: Button[][],
    subscribers: Subscriber[],
    codes: any,
    servers: any[],
}

interface Messages {
    welcome: string,
}

interface RssInfo {
    text: string,
    _id: string,
    uri: string
}

interface UserInfo {
    _id: string,
    vkId: number,
    displayName: string,
    bots: BotInfo[]
}

interface Subscriber {
    vkId: string,

    info: {
        name: string,
        family: string,
        avatar: string, 
    }

    /** Согласие на получение рассылки  */ 
    rss: boolean,
    steamId?: string,
} 

interface Broadcast {
    msg: string,
    attachment?: string,
}

interface Button {
    name: string,
    type: string, // TEXT, URL
    color: string,
    stats: ButtonStats,

    data: any
}

interface ButtonStats {
    click?: number
}

interface BillInfo {
    // Успешно ли создался запрос на пополнение
    success: boolean,
    // Ссылка на пополнение, в случае удачи, ошибка - в случае провала
    data: string
}

interface PaymentVariant {
    sum: number,
    multiplier: number,
}

interface ButtonType {
    name: string,
    code: string
}


interface Player {
    servers: any,
    steamId: string,
}

interface Stats {
    kills: number,
    death: number,

    farm: {
        'sulfur.ore': number, 
        'stones': number, 
        'metal.ore': number,
        'hqm': number,
    }
}

interface ProgressBar {
    progress: number,
    errors: number,
    success: number, 
}

// -------------------------------------------------------------------------
// Объявление констант
// -------------------------------------------------------------------------
declare const app: App;
declare const logger: import('winston').Logger;
declare const webApi: any;
declare const showAlert: any;
(window as any).webApi = any;
(window as any).showAlert = any;
(<any>window).showAlert = any;
(<any>window).router = import('vue-router');