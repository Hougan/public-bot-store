const serverLine = require('serverline');
const Bill = require('../main/classes/Bill');
const Bot = require('../main/classes/Bot');
const Api = require('../main/classes/API');

module.exports = (async () => {
    process.stdout.write("\x1Bc")
    console.log(Array(process.stdout.rows + 1).join('\n'));

    serverLine.init();
    serverLine.setCompletion(['status']);

    serverLine.setPrompt('> ');

    serverLine.on('line', (line) => {
        switch (line) {
          case "clear": {
            process.stdout.write("\x1Bc")
            console.log(Array(process.stdout.rows + 1).join('\n'));
            break;
          }
          case 'stop': {
            Bot.botPoll[0].turnOff();
            break;
          }
          case 'start': {
            Bot.botPoll[0].turnOn();
            break;
          }
          case 'status': {
            console.log(Bot.botPoll[0].isStarted());
            break;
          }
          default: {
            Api.checkApi('b1779e00874f804764ff6cc2ba58e78425b5b648fe2a06aca8062a7fa707a3870d01b45f45306df28c0c6');
            logger.error('Unknown command!');
          }
        }
      })
})();
