const Bill = require('../main/classes/Bill');
const { ObjectId } = require('mongodb');
const User = require('../main/classes/User');

module.exports = async () => {
  if (!app.socketServer){ 
    logger.error('Не подключен сокет сервер!');
    return;
  }

  app.socketServer.on('bill variants', async (/** @type {SocketIO.Socket} */ socket,  /** @type {null} */ data, /** @type {Express.Session} */  session) => {
      return Bill.variants;
  });

  app.socketServer.on('bill create', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    const { amount } = data;
    if (!amount) {
        return {
            success: false,
            data: 'Не выбрана сумма пополнения'
        };
    }

    const variant = Bill.variants.find(v => v.sum == amount);
    if (!variant) {
        return {
            success: false,
            data: 'Неизвестная сумма пополлнения'
        }; 
    }
    
    const { user: origUser } = session;
    if (!origUser) {
        return {
            success: false,
            data: 'Неизвестная ошибка №1'
        };
    }

    const user = await User.find(origUser.vkId);
    if (!user) {
        return {
            success: false,
            data: 'Неизвестная ошибка №2'
        };
    }

    const result = await Bill.create({
        id: user.id,
        name: user.displayName,
    }, variant);
    if (!result) {
        return {
            success: false,
            data: 'Неизвестная ошибка №3'
        };
    }

    return {
        success: true,
        data: result.link
    };
  });
};