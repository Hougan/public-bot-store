const Bot = require('../main/classes/Bot');
const User = require('../main/classes/User');
const { ObjectId } = require('mongodb');
const Api = require('../main/classes/API');

module.exports = async () => {
  if (!app.socketServer){ 
    logger.error('Не подключен сокет сервер!');
    return;
  }

  app.socketServer.on('bot create', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    // Нельзя создавать бота без сессии
    if (!session.user) {
      return;
    }

    const { token } = data;
    if (!token) {
      return 'Вы не указали ваш API-ключ';
    }

    const keyInfo = await Api.checkApi(token);
    if (!keyInfo.status) {
      return 'Неверно указан ключ, или у ключа отсутствуют права доступа!';
    }

    const bot = await Bot.create(keyInfo.name, keyInfo.avatar, token);
    if (!bot || typeof bot != 'object') {
      return bot;
    }

    const { vkId } = session.user;

    const user = await User.find(vkId);
    if (!user) {
      return 'Неизвестная ошибка'; 
    }

    if (!user.data.bots){
      user.data.bots = [];
    }

    user.data.bots.push(bot.id);
    session.user = user.data;
    session.save(() => {});
    user.save();

    return true;
  });

  app.socketServer.on('bot save', async (/** @type {SocketIO.Socket} */ socket,  /** @type {BotInfo} */ data, /** @type {Express.Session} */  session) => {
    const { name, _id, buttons } = data;
    if (!name || !_id || !buttons) {
      return 'Указаны неверные параметры!';
    }

    delete (data.statistics);
    delete (data.subscribers);
    delete (data.codes);

    const exists = Bot.findById(new ObjectId(_id));
    if (!exists) {
      return 'Бот не найден';
    }

    delete (data._id);

    Object.assign(exists.data, data); 
    exists.save();

    await exists.restart();
    return true;
  });

  app.socketServer.on('bot delete', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    const { _id, token } = data;
    console.log(data);
    if (!_id || !token) {
      return 'Убедитесь, что ввели token!';
    }

    const exists = Bot.findById(new ObjectId(_id));
    if (!exists) {
      return "Бот с таким идентификатором не найден";
    }

    await exists.selfDestroy();

    return true;

  });
};