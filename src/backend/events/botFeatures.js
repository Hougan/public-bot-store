const Bot = require('../main/classes/Bot');
const { ObjectId } = require('mongodb');

module.exports = async () => {
  if (!app.socketServer){ 
    logger.error('Не подключен сокет сервер!');
    return;
  }

  app.socketServer.on('bot download plugin', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    if (!data || !data._id) {
      console.log(data);
      logger.error('Неправильная перегрузка при загрузке плагина');
      return 0;
    }

    const fs = require('fs');
    let buffer = await fs.readFileSync('./protected/plugin/VKBot.cs');
    
    // Переводим буффер в строку
    const result = buffer.toString().replace('%BOTID%', data._id).replace('%SERVERID%', (new ObjectId()).toString());
    return result;
  });

  app.socketServer.on('bot remove server', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    if (!data || !data._id || !data.serverId) {
      return 'Неправильная перегрузка при загрузке плагина';
    }

    const bot = Bot.findById(new ObjectId(data._id));
    if (!bot) {
      return 'Бот не найден в пулле!';
    }

    if (!bot.servers[data.serverId]) {
      return 'Сервер не найден!';
    }

    delete (bot.servers[data.serverId]);

    bot.statistics.forEach(user => {
      if (user.servers[data.serverId]) {
        delete user.servers[data.serverId];
      }
    });

    bot.save();

    return true;
  });

  app.socketServer.on('bot status', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    if (!data || !data._id) {
      return 0;
    }

    const bot = Bot.findById(new ObjectId(data._id));
    if (!bot) {
      return 0;
    }

    return bot.isStarted() ? 1 : -1;
  });

  app.socketServer.on('bot start', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    if (!data || !data._id) {
      return false;
    }

    const bot = Bot.findById(new ObjectId(data._id));
    if (!bot) {
      return false;
    }

    const result = await bot.canStart();
    if (!result) {
      bot.turnOn(); 
    }

    return result;
  });

  app.socketServer.on('bot stop', async (/** @type {SocketIO.Socket} */ socket,  /** @type {any} */ data, /** @type {Express.Session} */  session) => {
    if (!data || !data._id) {
      return false;
    }

    const bot = Bot.findById(new ObjectId(data._id));
    if (!bot) {
      return false;
    }

    bot.turnOff();
    return true;
  });

  app.socketServer.on('bot broadcast', async (/** @type {SocketIO.Socket} */ socket,  /** @type {RssInfo} */ data, /** @type {Express.Session} */  session) => {
    const { _id, msg, attachment } = data;
    if (!msg || !_id) {
      return 'Убедитесь, что вы ввели текст';
    }

    const bot = Bot.findById(new ObjectId(_id));
    if (!bot) {
      return 'Бот не найден (выключен или не создан)';
    }

    bot.broadcast(msg, socket, attachment);
    return true;
  });
};