const Bot = require("./Bot");
const VKApi = require('node-vk-bot-api/lib/api');

class Api {
    /**
     * Ответить на запрос
     * @param {boolean} success 
     * @param {any} data 
     */
    static answer(success, data) {
        return JSON.stringify({ success, data });
    }

    /**
     * Проверить АПИ-ключ
     * @param {string} token
     * @returns {Promise<{ status: boolean, avatar: string, name: string }>}
     */
    static async checkApi(token) {
        await new Promise((callback) => {
            setTimeout(() => callback(), 1000);
        })

        try {
            let result = await VKApi('groups.getById', {
                access_token: token
            });

            const name = result.response[0].name;
            const avatar = result.response[0].photo_100;
            const groupId = result.response[0].id;
            
            result = await VKApi('groups.getLongPollServer', {
                group_id: groupId,
                access_token: token,
            });

            return {
                status: true,
                avatar: avatar,
                name: name
            }
        }
        catch(err) {
            return {
                status: false,
                avatar: '',
                name: ''
            }
        }
    }

    /**
     * Получить код для пользователя
     * @param {string} token
     * @param {string} userId
     * @returns {string|boolean}
     */
    static getCode(token, userId) {
        const bot = Bot.find(token);
        if (!bot) {
            return false;
        }

        return bot.getCode(userId);
    }

    /**
     * Применить статистику игрока на конкретном сервере
     * @param {string} token 
     * @param {string} steamId 
     * @param {PlayerServer} server 
     */
    static applyStats(token, steamId, server) {
        const bot = Bot.find(token);
        if (!bot) {
            return false;
        }

        const result = bot.applyStats(steamId, server);
        if (result) {
            bot.save();
        }

        return result;
    }
}

module.exports = Api;