const { ObjectId } = require('mongodb');

const MongoDB = require('@backend/main/classes/MongoDB');
const Markup = require('node-vk-bot-api/lib/markup');
const Api = require('node-vk-bot-api/lib/api');
const VkBot = require('node-vk-bot-api');

const base = new MongoDB('bots');

class Bot {
    /**
     * Список инициализированных ботов
     * @type {Bot[]}
     */
    static botPoll = [];

    /**
     * Создать бота в базе данных
     * @param {string} name
     * @param {string} avatar
     * @param {string} token
     * @returns {Promise<Bot|string|null>}
     */
    static async create(name, avatar, token) {
        const result = await base.findOne({ token });
        if (result) {
            return 'Бот с таким индентификатором уже создан';
        }

        const { insertedId } = await base.insertOne({
            name: name,
            avatar: avatar,
            token: token,

            messages: {
                welcome: 'Привет, %USER_NAME%. Чем я могу тебе помочь?' 
            },

            "buttons" : [ 
                [ 
                    {
                        "name" : "Правила ⛔",
                        "type" : "text",
                        "color" : "negative",
                        "data" : {
                            "msg" : "1. Запрещено использование любых читов, багов, недоработок игры или сервера, стороннего ПО дающего игровое преимущество. Запрещено играть в группе с читером. Наказание: бан.",
                            "attachment" : "https://vk.com/playrustvk?z=photo-163921999_457241170%2Falbum-163921999_268393206"
                        },
                        "stats" : {
                            "click" : 1
                        }
                    }, 
                    {
                        "name" : "Когда вайп? 📅",
                        "type" : "wipe",
                        "color" : "positive",
                        "data" : {},
                        "stats" : {
                            "click" : 2
                        }
                    }, 
                    {
                        "name" : "Бинды ⌨",
                        "type" : "text",
                        "color" : "primary",
                        "data" : {
                            "msg" : "/info - Вызов справочной системы/информация о сервере\n/q - Открыть меню квестов\n/s - Открыть внутриигровой магазин"
                        }
                    }
                ], 
                [ 
                    {
                        "name" : "Подключиться к серверу 💻",
                        "type" : "uri",
                        "color" : "default",
                        "data" : {
                            "uri" : "https://google.ru"
                        }
                    }, 
                    {
                        "name" : "Магазин 💰",
                        "type" : "uri",
                        "color" : "default",
                        "data" : {
                            "uri" : "https://google.ru"
                        }
                    }
                ], 
                [ 
                    {
                        "name" : "Рассылка о новостях и вайпах 📰",
                        "type" : "subscribe",
                        "color" : "default",
                        "data" : {
                            "sub_start" : "Привет! Хочешь первым получать информацию о вайпах? Подпишись на рассылку",
                            "sub_yes" : "Спасибо за подписку!",
                            "sub_status" : "Ты уже подписан, неужели хочешь отписаться?",
                            "unsub_yes" : "Жаль, будем ждать твоего возвращения :)"
                        }
                    }
                ], 
                [ 
                    {
                        "name" : "FAQ",
                        "type" : "text",
                        "color" : "positive",
                        "data" : {
                            "msg" : "❓ Можно ли зайти на сервер с пиратской версии игры?\n✔ Нельзя, на наших серверах поддерживается только лицензионная версия игры."
                        }
                    }
                ]
            ],
            subscribers: [],
            codes: {}
        });

        return await Bot.init(insertedId.toString());
    }

    /**
     * Найти бота по токену
     * @param {string} token
     * @returns {Bot|undefined} 
     */
    static find(token) {
        const result = Bot.botPoll.find(v => v.token == token);
        return result;
    }

    /**
     * Найти бота по ID
     * @param {ObjectId} id
     * @returns {Bot|undefined} 
     */
    static findById(id) {
        const result = Bot.botPoll.find(v => v.id.toString() == id.toString());
        return result;
    }

    /**
     * Инициализировать бота из БД
     * @param {string} id
     * @returns {Promise<Bot|null>}
     */
    static async init(id) {
        const result = await base.findOne({ _id: new ObjectId(id) });
        if (!result) {
            logger.error(`Не удалось найти бота с ID: '${id}'`);
            return null;
        }

        return new Bot(result);
    }

    /**
     * Инициализировать всех ботов из БД
     */
    static async initAll() {
        const result = await base.aggregate([]);
        result.forEach(v => Bot.init(v._id.toString()));
    }

    /**
     * Конструктор класса
     * @param {any} value
     */
    constructor(value) {
        this._ = {
            /** @type {any} */
            bot: null
        };
        this.data = value;

        Bot.botPoll.push(this);

        this.turnOn();

        setInterval(async () => {
            const result = await this.canStart();

            if (result && this.isStarted()) {
                await this.turnOff(); 
            }
        }, 30000);
    }

    /** 
     * ID бота в системе 
     * @type {ObjectId}
     */
    get id() { return new ObjectId(this.data._id); }

    /**
     * Список подключенных серверов
     */
    get servers() { 
        if (!this.data.servers)  {
            this.data.servers = {};
        }

        return this.data.servers;
    }

    set servers(v) { 
        this.data.servers = v;
    }

    /**
     * Название бота / проекта
     * @type {string}
     */
    get name() { return this.data.name; }

    set name(v) { this.data.name = v; }

    /**
     * Дата до которой оплачен бот
     */
    get payUntil() { return this.data.payUntil; }

    set payUntil(v) { this.data.payUntil = v; }

    /**
     * Токен от группы ВК
     */
    get token() { return this.data.token; }

    set token(v) { this.data.token = v; }

    /**
     * Сообщения от группы
     * @type {Messages}
     */
    get messages() { return this.data.messages; }

    set messages(v) { this.data.messages = v; }

    /**
     * Инстанс рабочего бота
     * @type {any}
     */
    get bot() { return this._.bot; }

    set bot(v) { this._.bot = v; }

    /**
     * Список подписчиков
     * @type {Subscriber[]}
     */
    get subscribers() { 
        this.data.subscribers = this.data.subscribers || [];
        return this.data.subscribers;
    }

    set subscribers(v) { this.data.subscribers = v; }

    /**
     * Коды пользователей
     * @type {any}
     */
    get codes() {
        this.data.codes = this.data.codes || {};
        return this.data.codes;
    }

    set codes(v) { this.data.codes = v; }

    /**
     * Кнопки доступные в боте
     * @type {Button[][]}
     */
    get buttons() { 
        this.data.buttons = this.data.buttons || [];
        return this.data.buttons; 
    }

    set buttons(v) { this.data.buttons = v; }

    /**
     * Статистика пользователей
     * @type {Player[]}
     */
    get statistics() {
        this.data.statistics = this.data.statistics || [];
        return this.data.statistics;
    }

    set statistics(v) { this.data.statistics = v; } 
 
    /**
     * Инициализирован ли бот
     * @returns {boolean}
     */
    isStarted() {
        return !!this.bot;
    }

    /**
     * Включить бота 
     */
    async turnOn() {        
        if (this.isStarted()) {
            await this.turnOff();
        }

        if (!this.canStart()) {
            return;
        }

        this.bot = new VkBot(this.token);

        this.subCommands();
        this.bot.startPolling();

        logger.warn(`Запущен бот: ${this.token}`);
    }
 
    /**
     * Выключить бота
     */
    async turnOff() {
        if (this.isStarted()) {
            this.bot.stop();
            this.bot = null;
        }

        logger.warn(`Остановлен бот: ${this.token}`);
    }

    /** 
     * Рестарт бота
     */
    async restart() { 
        await this.turnOff();

        await this.turnOn();    
    }

    subCommands() {  
        this.bot.command('/start', async (/** @type {any} */ ctx) => { 
            this.sendMenu(ctx, null);
        });

        this.buttons.forEach(line => {
            line.forEach(btn => {
                this.bot.command(btn.name, this.getAction(btn));
            });
        });

        // Проверяем нажатие на кнопки (вспомогательные да/нет и другие)
        this.bot.on(async (/** @type {any} */ ctx, /** @type {any} */ next) => {
            if (!ctx.message || !ctx.message.payload) {
                next();
                return;
            }

            const payload = JSON.parse(ctx.message.payload);
            if (!payload || !payload.button) { 
                logger.error('Неправильная перегрузка в доп. командах');
                next();
                return;
            }

            const { type, action } = payload.button;
            if (!type || !action) {
                next();
                return;
            }

            switch (type.toLowerCase()) {
                case 'subscribe': {
                    this.subscribeProcess(ctx);
                    break;
                }
            }

            next();
        });
        
        // Проверяем отправленность кода
        this.bot.on(async (/** @type {any} */ ctx, /** @type {any} */ next) => {
            if (!ctx.message || !ctx.message.text) {
                next();
                return;
            } 

            const { text } = ctx.message;
            const number = parseInt(text);

            if (isNaN(number) || text.length != 6) {
                next();
                return;
            }

            let sub = this.getSubFromCtx(ctx);
            if (!sub) {                
                const info = await this.getNameByCtx(ctx);

                this.subscribers.push({
                    vkId: ctx.message.from_id,
                    info: info,
                    rss: false
                });
                sub = this.getSubFromCtx(ctx);

                if (!sub) {
                    logger.error('Странная ошибка добавления пользователя в лист подписчиков');
                    next();
                    return;
                }
            }

            if (sub.steamId) {
                ctx.reply('У вас уже привязан аккаунт STEAM. Отвязать его нельзя!');
                return;
            }

            const targetId = Object.entries(this.codes).find(([key, value]) => value == text);
            if (!targetId) {
                ctx.reply('Вы указали не действительный код!');
                return;
            }

            ctx.reply('Вы успешно привязали свой аккаунт steam!');

            // Приравниваем к подписчику аккаунт Steam
            sub.steamId = targetId[0];
            // Удаляем его аккаунт из ожидающих
            delete this.codes[targetId[0]];

            this.save();
            next();
        });
    }

    /**
     * Может ли бот запуститься
     * @returns {Promise<string|null>}
     */
    async canStart() {
        const { payUntil } = this;

        // Если текущее время меньше оплаченного, значит можно
        if (new Date().getTime() < payUntil) {
            return null;
        }

        const owner = await this.getOwner();
        if (!owner) {
            //logger.error('Неизвестная ошибка определения владельца');
            return 'Неизвестная ошибка определения владельца';
        }


        const sum = this.getCost();
        if (owner.balance < sum) {
            //logger.error('Недостаточно средств для оплаты бота');
            return 'Недостаточно средств для оплаты бота';
        }

        owner.balance -= sum;
        owner.save();

        this.payUntil = new Date().getTime() + 86400 * 1000;
        this.save();
    }

    /**
     * Получить стоимость работы бота за сутки
     * @returns {number}
     */
    getCost() {
        return 10;
    }

    /** 
     * Получить владельца бота
     * @returns {Promise<User|undefined>}
     */
    async getOwner() {
        const user = await require('./User').findByBot(this);

        return user;
    }

    /**
     * Записать клик по клавише
     * @param {Button} btn
     * @param {string} type 
     */
    applyStat(btn, type) {
        if (!btn.stats) {
            btn.stats = {};
        }

        if (!/** @type {any} */(btn.stats)[type]) {
            /** @type {any} */(btn.stats)[type] = 0;
        }

        /** @type {any} */(btn.stats)[type]++;
        this.save();
    }

    /**
     * Обработать соединение с сервером
     * @param {string} serverId 
     * @param {string} serverName 
     * @param {string} serverIp
     */
    fetchServer(serverId, serverName, serverIp) {
        const { servers } = this;

        if (!servers.hasOwnProperty(serverId)) {
            servers[serverId] = {
                wipes: [],
            }
        }

        const info = servers[serverId];
        info.name = serverName;

        if (!info.ip || info.ip != serverIp) {
            logger.error(`Смена IP для сервера: ${serverId}`);
            info.ip = serverIp;
        }

        info.lastUpdate = new Date();
        this.save();
    }

    /**
     * Получить действие для кнопки
     * @param {Button} btn
     */
    getAction(btn) {
        switch (btn.type.toLowerCase()) {
            case 'stats': {
                return (/** @type {any} */ ctx) => {
                    this.applyStat(btn, 'click'); 
                    const { from_id } = ctx.message;

                    const sub = this.subscribers.find(v => v.vkId == from_id);
                    if (!sub || !sub.steamId) {
                        ctx.reply(btn.data.no_steam);
                        return; 
                    }
 
                    const player = this.statistics.find(v => v.steamId == sub.steamId);
                    if (!player) {
                        ctx.reply('Статистика ещё не загружена, перезайдите на сервер');
                        return;
                    }

                    const baseMessage = `Ваша статистика на сервере — %SERVER_NAME%

                    🔪 Убийства: %KILLS%
                    💀 Смерти: %DEATHES%

                    Статистика добычи ресурсов:
                    ⛏ Сера: x%FARM.SULFUR_ORE%
                    ⛏ Камни: x%FARM.STONES% 
                    ⛏ Металл: x%FARM.METAL.FRAGMENTS%
                    ⛏ МВК: x%FARM.HQM%
                    🌳 Дерево: x%FARM.WOOD%`;
 
                    Object.entries(player.servers).forEach(async ([name, value]) => {  
                        const formatMessage = baseMessage
                            .replace('%SERVER_NAME%', name)
                            .replace('%KILLS%', value.kills.toString())
                            .replace('%DEATHES%', value.deathes.toString())
                            .replace('%FARM.SULFUR_ORE%', value.farm["sulfur.ore"].toString())
                            .replace('%FARM.STONES%', value.farm.stones.toString())
                            .replace('%FARM.METAL.FRAGMENTS%', value.farm["metal.ore"].toString())
                            .replace('%FARM.HQM%', value.farm['hq.metal.ore'].toString()) 
                            .replace('%FARM.WOOD%', value.farm.wood.toString())

                        await ctx.reply(formatMessage);  
                    });
                };
            } 
            case 'text': {
                return (/** @type {any} */ ctx) => {
                    this.applyStat(btn, 'click'); 
                    ctx.reply(btn.data.msg.toString(), btn.data.attachment);
                }
            }
            case 'subscribe': {
                return (/** @type {any} */ ctx) => {
                    this.applyStat(btn, 'click'); 
                    this.subscribeProcess(ctx);
                }; 
            }
            case 'wipe': {
                return ((/** @type {any} */ ctx) => {
                    this.applyStat(btn, 'click');
                    this.processWipe(ctx);
                });
            }
        }
    }

    /**
     * Отправить информацию о вайпах пользователю
     * @param {any} ctx 
     */
    async processWipe(ctx) {
        Object.entries(this.servers).forEach(async ([key, server], index) => {
            const wipe = this.getNextWipe(server);

            await ctx.reply(
                `${index + 1}. ${server.name} — следущий вайп будет ${wipe.date}`,
                null,
                Markup.keyboard([
                    [Markup.button(wipe.typeText, wipe.type)]
                ]).inline()
            );
        })
    }

    /**
     * Получить данные о следущем вайпе
     * @param {any} server 
     */
    getNextWipe(server) {
        console.log(server.wipes)
        let wipes = server.wipes.map((/** @type {any} */ v) => {
            const newDate = new Date(v.date);
            newDate.setHours(12);

            return {
                date: newDate,
                type: v.type
            }
        });

        wipes = wipes.sort((a, b) => a.date - b.date);

        console.log(wipes)
        /** @type {(Date|null)} */
        const nextWipe = wipes.find(v => new Date() < v.date);
        if (!nextWipe) {
            return {
                typeText: 'Не удалось определить дату',
                type: 'negative',
                date: '*НЕИЗВЕСТНО*'
            }
        }


        console.log(nextWipe);
        const dif = nextWipe.date.getTime() - new Date().getTime();
        
        console.log(dif);
        let time = '*НЕИЗВЕСТНО*';
        const days = dif / (86400 * 1000);
        console.log(days);
        if (days < 1) {
            time = 'сегодня';
        } else if (days > 1 && days < 2) {
            time = 'завтра';
        } else if (days > 2 && days < 3) {
            time = 'послезавтра';
        } else {
            time = `через ${Math.floor(days)} дней`
        }

        return {
            typeText: nextWipe.type == 0 ? 'Карта, без изучений' : 'Глобальный, вместе с изучениями',
            type: nextWipe.type == 0 ? 'default' : 'primary',
            date: time
        } 
    }

    /** 
     * Получить имя/фамилию по CTX
     * @param {any} ctx 
     */
    async getNameByCtx(ctx) {
        const exists = this.subscribers.find(v => v.vkId == ctx.message.from_id);
        if (exists) {
            return exists;
        }

        const result = await Api('users.get', {
            user_ids: ctx.message.from_id,
            access_token: this.token,
            fields: ['photo_100']
        });

        if (!result || !result.response || result.response.length == 0) {
            logger.error('Получены неверные данные об имени!');
            return {
                name: 'Unknown',
                family: 'Unknown',
                avatar: ''
            };
        }

        const { first_name, last_name, photo_100 } = result.response[0];

        const info = {
            name: first_name,
            family: last_name,
            avatar: photo_100
        };

        this.subscribers.push({
            vkId: ctx.message.from_id,
            info: info,
            rss: false
        });

        return await this.getNameByCtx(ctx);
    }

    /** 
     * Обработать нажатие на подписку
     * @param {any} ctx
     */
    async subscribeProcess(ctx) {
        const { from_id, text, payload: rawPayload } = ctx.message; 

        // Ищем строку, в которой есть кнопка подписаться
        let line = this.buttons
            .find(l => l.find(b => b.type == 'subscribe'));
        if (!line) {
            logger.error('Не найдена кнопка с необходимым типом для подписки!');
            return;
        }

        // Находим кнопку в линии
        const btn = line.find(v =>  v.type == 'subscribe');
        if (!btn) {
            logger.error('Не найдена кнопка с необходимым типом для подписки!');
            return;
        }

        if (!rawPayload) {
            logger.info('Неправильная перегрузка команды');
            return;
        }

        const payload = JSON.parse(rawPayload);
        if (!payload || !payload.button) {
            logger.info('Неправильная перегрузка команды');
            return;
        }

        const { type, action } = payload.button;

        const sub = this.getSubFromCtx(ctx);

        switch(action.toLowerCase()) {
            // Нажатие на кнопку "подписаться"
            case 'start': {
                // Если пользователь не подписан
                if (!sub || !sub.rss) {
                    ctx.reply(/** @type {ButtonSubscribeData} */ (btn.data).sub_start, null, Markup.keyboard([
                        [
                            Markup.button('Да', 'positive', {
                                button: {
                                    type: 'subscribe',
                                    action: 'sub_yes'
                                }
                            }),
                            Markup.button('Нет', 'negative'), 
                        ]
                    ]).inline()).catch((/** @type {string|null} */ err) => {
                        ctx.reply('Техническая ошибка, свяжитесь с администратором. Ошибка: ' + JSON.stringify(err)); 
                    });;
                    return;
                }

                ctx.reply(/** @type {ButtonSubscribeData} */ (btn.data).sub_status, null, Markup.keyboard([
                    [
                        Markup.button('Да', 'negative', {
                            button: {
                                type: 'subscribe',
                                action: 'unsub_yes'
                            }
                        }),
                        Markup.button('Нет', 'positive', undefined),
                    ]
                ]).inline()).catch((/** @type {string|null} */ err) => {
                    ctx.reply('Техническая ошибка, свяжитесь с администратором. Ошибка: ' + JSON.stringify(err)); 
                });;
                break;
            }

            case 'sub_yes': {
                if (!sub) { 
                    const info = await this.getNameByCtx(ctx);

                    this.subscribers.push({
                        vkId: from_id,
                        info: info,
                        rss: true
                    });
                } else {
                    if (sub.rss) { 
                        ctx.reply('Вы уже подписаны на рассылку');
                        return;
                    }

                    sub.rss = true;
                }

                this.sendMenu(ctx, /** @type {ButtonSubscribeData} */ (btn.data).sub_yes);
                this.save();
                break;
            }

            case 'unsub_yes': {
                if (!sub || !sub.rss) { 
                    ctx.reply('Вы не подписаны на рассылку.');
                    return;
                }

                sub.rss = false;

                this.sendMenu(ctx, /** @type {ButtonSubscribeData} */ (btn.data).unsub_yes);
                this.save();
                break;
            }
        }
    }

    /**
     * Отправляет меню пользователю 
     * @param {any} ctx 
     * @param {string|undefined|null} message 
     */
    async sendMenu(ctx, message) {
        if (!message) {
            message = this.messages.welcome.toString();
        }

        const result = await this.getNameByCtx(ctx);
        message = message.replace("%USER_NAME%", result.info.name);


        const markup = this.buttons.map(line => {
            return line.map(btn => {
                return this.getBtn(ctx, btn);
            });
        });

        ctx.reply(message, null, Markup.keyboard(markup)) 
            .catch((/** @type {string|null} */ err) => {
                ctx.reply('Техническая ошибка, свяжитесь с администратором. Ошибка: ' + JSON.stringify(err)); 
            });
    }

    /**
     * Получить экземпляр подписчика из CTX
     * @param {any} ctx 
     */
    getSubFromCtx(ctx) {
        return this.subscribers.find(v => v.vkId == ctx.message.from_id);;
    }

    /**
     * Получить рабочую кнопку из данных
     * @param {any} ctx
     * @param {Button} value
     * @returns {any}
     */
    getBtn(ctx, value) {
        const sub = this.getSubFromCtx(ctx);

        switch (value.type.toLocaleLowerCase()) {
            case 'wipe': 
            case 'text':
            case 'stats':
                return Markup.button(value.name, value.color);
            case 'uri':
                return Markup.button({
                    action: {
                        type: 'open_link',
                        link: value.data.uri || value.data.msg,
                        label: value.name,
                        payload: JSON.stringify({
                            url: value.data.uri || value.data.msg
                        }),
                    }
                });
            case 'subscribe':
                return Markup.button(
                    value.name,
                    (!sub || !sub.rss ? 'positive' : 'default'),
                    {
                        button: {
                            type: 'subscribe',
                            action: 'start'
                        }
                    }
                );
        }
    }

    /**
     * Получить код для пользователя
     * @param {string} userId 
     * @returns {string|false}
     */
    getCode(userId) {
        const sub = this.subscribers.find(v => v.steamId == userId);
        if (sub) {
            return false;
        }

        const code = this.codes[userId];
        if (!code) {
            this.codes[userId] = this.generateCode();
            this.save();

            return this.getCode(userId);
        }

        return code;
    }

    /**
     * Отправить рассылку пользователям
     * @param {string} message
     * @param {SocketIO.Socket} socket
     * @param {string} attachment
     */
    async broadcast(message, socket, attachment = '') {
        const base = this.subscribers.filter(v => v.rss);
        const subs = base.map(v => v.vkId);

        while (subs.length > 0) {
            const part = subs.splice(0, 95);
            const result = await this.bot.sendMessage(part, message, attachment)
                .catch((/** @type {any} */ err) => console.log(err));

            if (result) {
                const progress = (1 - subs.length / base.length) * 100;
     
                socket.emit('bot broadcast status', {
                    progress: progress,
                    errors: result.filter((/** @type {any} */ v) => v.error && v.error.length > 0).length,
                    success: result.filter((/** @type {any} */ v) => !v.error).length
                });
            }
        }

        return true;
    }

    /**
     * Сгенерировать случайный код
     * @returns {string}
     */
    generateCode() {
        let code = (Math.random() * 1000000).toFixed(0);
        if (Object.entries(this.codes).find(([key, value]) => value == code)) {
            return this.generateCode();
        }

        return code;
    }

    /**
     * Полностью удалить бота
     */
    async selfDestroy() {
        if (this.isStarted()) {
            await this.turnOff();
        }

        const index = Bot.botPoll.indexOf(this);
        Bot.botPoll.splice(index, 1);

        base.deleteOne({ _id: this.id });
    }

    /**
     * Сохранить информацию о боте в БД
     */
    async save() {
        base.updateOne({ _id: this.id }, { $set: { ...this.data }});
    }
}

module.exports = Bot;