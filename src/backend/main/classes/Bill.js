const { ObjectId } = require('mongodb');

const User = require('./User');
const MongoDB = require('@backend/main/classes/MongoDB');
const QiwiBillPaymentsAPI = require('@qiwi/bill-payments-node-js-sdk');

const base = new MongoDB('bills');

class Bill {
    /** @type {Bill[]} */ 
    static billPoll = [];

    /**
     * Секретный ключ от QIWI
     * @type {string}
     */
    static secretKey = ``;

    /**
     * Публичный ключ от QIWI
     * @type {string}
     */
    static publicKey = ``;

    /**
     * Возможные варианты пополнения
     * @type {PaymentVariant[]}
     */
    static variants = [{
        sum: 150,
        multiplier: 1.0,
    }, {
        sum: 300,
        multiplier: 1.05,
    }, {
        sum: 500,
        multiplier: 1.1,
    }, {
        sum: 1000,
        multiplier: 1.2,
    }];

    /**
     * Создать счёт на пополнение баланса
     * @param {{ id: ObjectId, name: string }} user 
     * @param {PaymentVariant} variant
     * @returns {Promise<Bill|null>}
     */
    static async create(user, variant) {
        const now = new Date();
        now.setMinutes(now.getMinutes() + 5);
        now.setHours(now.getHours() + 3);

        const time = (now).toISOString().split(':');
        
        const result = await base.insertOne({
            variant: variant,
            user: user,
            status: 'WAITING',
            time: time[0] + time[1]
        });

        if (!result) {
            logger.error('Ошибка создания запроса');
            return null;
        }

        logger.info(`Создан счёт на оплату для ${user.name} на сумму ${variant.sum} руб.`)
        return await Bill.init(result.insertedId);
    }


    /**
     * Инициализировать бота из БД
     * @param {string} id
     * @returns {Promise<Bill|null>}
     */
    static async init(id) {
        const result = await base.findOne({ _id: new ObjectId(id) });
        if (!result) {
            logger.error(`Не удалось найти платёж с ID: '${id}'`);
            return null;
        }

        return new Bill(result);
    }

    /**
     * Получить платежи конкретного пользователя
     * @param {ObjectId} id 
     * @returns {any[]}
     */
    static getUserBills(id) {
        const result = Bill.billPoll.filter(v => v.user && v.user.id && v.user.id.toString() == id.toString());

        const format = result.map(v => {
            return {
                status: v.data.status == 'PAID' 
                            ? 1 
                            : v.data.status == 'WAITING'
                                ? 0
                                : v.data.status == 'EXPIRED' 
                                    ? -2
                                    : -1,
                link: v.data.link,
                variant: v.data.variant,
                time: v.data._created
            };
        });

        return format;
    }

    /**
     * Инициализировать всех ботов из БД
     */
    static async initAll() {
        const result = await base.aggregate([]);
        result.forEach(v => Bill.init(v._id.toString()));
    }

    /**
     * Конструктор класса
     * @param {any} value 
     */
    constructor(value) {
        this.data = value;

        this.link = this.createLink();

        this.paymentTicker();
        this.save();

        Bill.billPoll.push(this);
    }

    /**
     * Индентификатор платежа
     * @type {ObjectId}
     */ 
    get id() { return new ObjectId(this.data._id) }

    /**
     * Статус платежа
     * @type {string}
     */
    get status() { return this.data.status; }
    
    set status(v) { this.data.status = v; }

    /**
     * Тип пополнения
     * @type {PaymentVariant}
     */
    get variant() { return this.data.variant; }

    set variant(v) { this.data.variant = v; }

    /**
     * Общая сумма платежа
     * @type {number}
     */
    get total() { 
        return this.data.variant.sum * this.data.variant.multiplier;
    }

    /**
     * Ссылка на оплату
     */
    get link() { return this.data.link; }

    set link(v) { this.data.link = v;}

    /**
     * Пользователь для платежа
     * @type {any}
     */
    get user() { return this.data.user; }

    set user(v) { this.data.user = v; }

    /**
     * Получить ссылку на оплату счёта
     * @returns {string}
     */
    createLink() {
        const qiwiApi = new QiwiBillPaymentsAPI(Bill.secretKey);

        const params = {
            publicKey: Bill.publicKey,
            amount: this.variant.sum,
            billId: this.id.toString(),
            successUrl: 'https://bot.hougan.space/',
            comment: `Пополнение баланса на ${this.variant.sum} руб. для аккаунта: ${this.user.name}`,
            lifetime: this.data.time
        }

        
        const result = qiwiApi.createPaymentForm(params);
        if (!result) {
            logger.error(`Ошибка инициализации платёжной формы для ${this.data.user} на сумму ${this.variant.sum}`);
            return 'UNKNOWN';
        }

        return result.toString();
    }

    async paymentTicker() {
        if (this.status != 'WAITING' && this.status != 'ERROR') {
            return;
        }

        // Проверяем оплату
        await this.checkPayment();

        // @ts-ignore
        if (this.status == 'PAID') { 
            logger.info(`Пополнение на сумму ${this.variant.sum} для аккаунте ${this.user.name}`);
            const user = await User.findByObjectId(this.user.id);
            if (!user) { 
                this.status = 'ERROR';
                logger.error('Неизвестная ошибка определения пользователя!');
            } else {
                user.balance += this.total;
                user.save();
            } 
        }

        setTimeout(() => this.paymentTicker(), 1000);
    } 

    async checkPayment() {
        const qiwiApi = new QiwiBillPaymentsAPI(Bill.secretKey);

        try {
            /** @type {any} */
            const result = await qiwiApi.getBillInfo(this.id.toString());

            this.status = result.status.value;
            this.save();
        }
        catch {
            return;
        }
    }

    /**
     * Сохранить информацию о боте в БД
     */
    async save() {
        base.updateOne({ _id: this.id }, { $set: { ...this.data }});
    }
}

module.exports = Bill;