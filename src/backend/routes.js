const Api = require("./main/classes/API");
const Bot = require("./main/classes/Bot");
const Auth = require("./main/classes/Auth");

const { ObjectId } = require('mongodb');


module.exports = async () => {
  if (!app.express){
    logger.error('Экспресс не загружен!');
    return;
  }

  // -------------------------
  // АПИ ответы на различные запросы
  // ------------------------- 

  /**
   * Middleware на проверку ID
   * @param {any} req 
   * @param {any} res 
   * @param {any} next 
   */
  const idCheck = (req, res, next) => {
    const { id, serverId, serverName } = req.body;
    if (!id || !serverId || !serverName) {
      res.end(Api.answer(false, 'Не указан ID для валидации'));
      return;
    }

    const bot = Bot.findById(new ObjectId(id));
    if (!bot) {
      res.end(Api.answer(false, 'Бот не найден!'));
      return;
    }

    const ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).replace('::ffff:', '');
    bot.fetchServer(serverId, serverName, ip);

    req.session.bot = bot;
    next();
  };

  app.express.post('/api/validate', idCheck, async (req, res) => {
    if (!req.session || !req.session.bot) {
      res.end(Api.answer(false, 'error #2251'));
      return;
    }

    const { bot } = req.session;

    res.end(Api.answer(true, bot.name));
  });

  app.express.post('/api/stats', idCheck, async (req, res) => {
    const { userId, stats, serverId } = req.body;
    if (!userId) {
      res.end(Api.answer(false, 'Переданы неверные параметры!'));
      return;
    }
    
    if (!req.session || !req.session.bot) {
      res.end(Api.answer(false, 'error #2251'));
      return;
    }

    const { bot } = req.session;

    let user = bot.statistics.find(v => v.steamId == userId);
    if (!user) {
      const acc = bot.subscribers.find(v => v.steamId == userId);
      if (!acc) {
        res.end(Api.answer(false, null));
        return;
      }

      bot.statistics.push({
        steamId: userId,
        servers: {}
      });

      user = bot.statistics.find(v => v.steamId == userId);
    }
 
    if (!user) {
      logger.error('Неизвестная ошибка получения пользователя для получения статистики!');
      return;
    }
    
    if (!stats) {
      res.end(Api.answer(!!user, user ? user.servers[serverId] : null));
      return;
    } 

    user.servers[serverId] = stats;
    await bot.save();

    res.end(Api.answer(true, 'SAVED'));
  });

  app.express.use((req, res, next) => {
    if (!req.user && !req.path.includes('auth')) {
      res.redirect('/auth/vkontakte');
      res.end();

      return;
    }

    req.session.user = req.user;
    next();
  });

  app.express.get('/api/user/getCode/:token/:steamId', async (req, res) => {
    const { steamId, token } = req.params;

    const result = Api.getCode(token, steamId);

    res.end(Api.answer(true, result));
  });


  app.express.post('/api/user/setStats/:token/:steamId', async (req, res) => {
    const { steamId, token } = req.params;
    const server = req.body;

    const result = Api.applyStats(token, steamId, server);
    res.end(Api.answer(true, result));
  });

  app.express.get('/api*', async (req, res) => {
    res.end(Api.answer(true, 'Module/section not found!'));
    return;
  });

  app.express.get('/auth/vkontakte', Auth.Passport().authenticate('vkontakte'))

  app.express.get('/auth/vkontakte/callback', 
    Auth.Passport().authenticate('vkontakte', {  
      successRedirect: '/index',
      failureRedirect: '/auth' 
    })
  );

  app.express.get('/auth', async (req, res) => {
    res.render('main');
  });

  app.express.get('/index', async (req, res) => {
    res.render('main');
  });

  app.express.get('/bot*', async (req, res) => {
    res.render('main');
  });     

  app.express.get('/broadcast*', async (req, res) => {
    res.render('main');
  });

  app.express.get('/balance', async (req, res) => {
    res.render('main');
  });
};
