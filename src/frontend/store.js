import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
      /**
       * Аккаунт пользователя в сервисе
       */
      user: null,

      /**
       * Ссылка на получение помощи по получению токена
       */
      getTokenUri: 'https://vk.com/@vkbotrust2-poluchenie-api-klucha',

      /**
       * Метка прокручиваемости прогресс бара
       */
      loading: false
  },
  mutations: {
    SET_USER: (state, payload) => {
        state.user = payload;
    },

    SET_LOADING: (state, payload) => {
      state.loading = payload;
    }
  },
  actions: {
    async FETCH_USER(context) {
      const result = await webApi.emit('user get');
      context.commit('SET_USER', result);
    }
  },
  getters: {
    USER: ({ user }) => user,
    HELP_TOKEN: ({getTokenUri}) => getTokenUri,
    LOADING: ({ loading }) => loading,
  }
});

