import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';
import '@mdi/font/css/materialdesignicons.min.css';

import App from '@frontend/main.vue';
import Auth from '@frontend/components/auth.vue';
import Menu from '@frontend/components/menu.vue';
import BotEditor from '@frontend/components/botEditor.vue';
import Balance from '@frontend/components/balance.vue';
import Broadcast from '@frontend/components/broadcast.vue';

import Vue from 'vue';
import Store from './store';
import Vuetify from 'vuetify'
import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';
import VueRouter from 'vue-router';

Vue.use(Vuetify);
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/index',
    component: Menu,
  }, {
    path: '/auth',
    component: Auth,
  }, {
    path: '/bot/:id',
    component: BotEditor,
  }, {
    path: '/balance',
    component: Balance,
  }, {
    path: '/broadcast/:id',
    component: Broadcast,
  }],
});

window.webApi = new WebApi.WebApiClient(IO('/'));
window.router = router;
new Vue({
  vuetify: new Vuetify({}),
  router,
  store: Store,
  render: (h) => h(App),
  async mounted() {
    await window.webApi.ready();

    // @ts-ignore
    this.$children[0].webApi = {};
    // @ts-ignore
    this.$children[0].webApi = window.webApi;

    // @ts-ignore
    if (this.$children[0].webApiReady) {
      // @ts-ignore
      this.$children[0].webApiReady();
    }
  },
}).$mount('#app');
