using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Oxide.Core.Libraries;
using Oxide.Game.Rust.Cui;

namespace Oxide.Plugins
{
    [Info("VKBot", "Hougan & Olkuts", "0.0.1")]
    [Description("Вспомогательный плагин для сервиса")]
    public class VKBot : RustPlugin
    {
        #region Classes

        private class Response
        {
            [JsonProperty("success")]
            public bool Success;
            [JsonProperty("data")]
            public object Data;
        }
        
        private class Request
        {
            public string Body;
            public string Module;

            public Request(string module, string body)
            {
                this.Module = module;
                this.Body = body;
            }

            public void Enqueue(Action<Response> callback)
            {
                Instance.webrequest.Enqueue(Uri + Module, Body, (i, s) =>
                {
                    Instance.PrintError(s);
                    try
                    {
                        Response response = JsonConvert.DeserializeObject<Response>(s);
                        callback(response);
                    }
                    catch
                    {
                        callback(new Response
                        {
                            Success = false,
                            Data = null,
                        });
                    }
                }, Instance, RequestMethod.POST, new Dictionary<string, string> { ["Content-Type"] = "application/json" });
            }

            public static string GetStats(string userId) => JsonConvert.SerializeObject(new
            {
                id = BotInfo.Id,
                serverId   = BotInfo.ServerId,
                serverName = ConVar.Server.hostname,
                
                userId = userId,
            });
            public static string SetStats(string userId, Stats stats) => JsonConvert.SerializeObject(new
            {
                id     = BotInfo.Id,
                serverId = BotInfo.ServerId,
                serverName = ConVar.Server.hostname,
                
                userId = userId,
                stats = stats
            });
            public static string GetCodeBody(string userId) => JsonConvert.SerializeObject(new
            {
                id     = BotInfo.Id,
                serverId   = BotInfo.ServerId,
                serverName = ConVar.Server.hostname,
                
                userId = userId
            });
            public static string ValidateBody() => JsonConvert.SerializeObject(new
            {
                id = BotInfo.Id,
                serverId = BotInfo.ServerId,
                serverName = ConVar.Server.hostname,
            });
        }
        
        private static class BotInfo
        {
            [JsonProperty("bot_name")]
            public static string Name = "Hougan Space";
            [JsonProperty("bot_id")]
            public static string Id = "%BOTID%";

            [JsonProperty("server_id")] 
            public static string ServerId = "%SERVERID%";

            public static void Validate(Action<bool> callback)
            {
                new Request("validate", Request.ValidateBody()).Enqueue(response =>
                {
                    callback(response.Success);
                });
            }
        }

        private class Configuration
        {
            [JsonProperty("Команда для привязки ВК")]
            public string AuthCommand = "vk";
        }
        
        private class Stats
        {
            [JsonProperty("kills")]
            public int Kills;
            [JsonProperty("deathes")]
            public int Deathes;
            
            [JsonProperty("farm")]
            public Dictionary<string, int> Farms = new Dictionary<string, int>();
        }

        #endregion

        #region Variables

        private static VKBot Instance;
        private static Configuration Settings = new Configuration();
        private static string Uri = "http://78.36.200.237:2020/api/";
        
        private static Hash<ulong, Stats> Statistics = new Hash<ulong, Stats>(); 

        #endregion

        #region Hooks

        private void OnServerInitialized()
        {
            Instance = this; 
            Statistics = new Hash<ulong, Stats>();
            
            BotInfo.Validate((success) =>
            { 
                PrintWarning($"Инициализация плагина VKBot");
                if (!success)
                {
                    PrintError($"Не удалось получить информацию о боте");
                    return;
                }
                
                PrintWarning("Плагин инициализирован успешно");
                
                BasePlayer.activePlayerList.ToList().ForEach(OnPlayerConnected);
                cmd.AddChatCommand(Settings.AuthCommand, this, nameof(AuthInterface));
            });
        }

        private void OnPlayerConnected(BasePlayer player)
        {
            Stats stats = null;

            if (Statistics.TryGetValue(player.userID, out stats))
            {
                return;
            }
            
            new Request("stats", Request.GetStats(player.UserIDString)).Enqueue((response) =>
            {
                if (!response.Success)
                {
                    PrintError($"Не удалось загрузить статистику игрока {player.userID}");
                    timer.Once(5, () => OnPlayerConnected(player));
                    return;
                }

                try
                {
                    stats = JsonConvert.DeserializeObject<Stats>(response.Data.ToString());
                }
                catch
                {
                    stats = new Stats();
                }
                
                if (!Statistics.ContainsKey(player.userID))
                {
                    Statistics.Add(player.userID, stats);
                }
            });
        }

        private void OnPlayerDisconnected(BasePlayer player, string reason = "")
        {
            Stats stats = null;

            if (!Statistics.TryGetValue(player.userID, out stats))
            {
                return;
            }
            
            new Request("stats", Request.SetStats(player.UserIDString, stats)).Enqueue((response) =>
            {
                if (!response.Success)
                {
                    PrintError($"Не удалось обновить статистику игрока {player.userID}");
                    return;
                }
            });
        }

        private void OnPlayerDeath(BasePlayer player, HitInfo info)
        {
            // Если есть инициатор начисляем ему убийство
            if (info != null && info.InitiatorPlayer != null)
            {
                Stats killStats = null;
                if (Statistics.TryGetValue(info.InitiatorPlayer.userID, out killStats))
                {
                    if (info.InitiatorPlayer.userID == player.userID)
                    {
                        return;
                    }
                    
                    killStats.Kills++;
                }
                
            }
            
            Stats dieStats = null;
            if (Statistics.TryGetValue(player.userID, out dieStats))
            {
                dieStats.Deathes++;
            }
        }

        private void OnDispenserGather(ResourceDispenser  entity, BasePlayer player, Item item)
        {
            Stats stats = null;
            if (!Statistics.TryGetValue(player.userID, out stats))
            {
                return;
            }

            if (!stats.Farms.ContainsKey(item.info.shortname))
            {
                stats.Farms.Add(item.info.shortname, 0);
            }

            stats.Farms[item.info.shortname] += item.amount;
        }
        
        private void OnDispenserBonus(ResourceDispenser dispenser, BasePlayer player, Item item)
        {
            Stats stats = null;
            if (!Statistics.TryGetValue(player.userID, out stats))
            {
                return;
            }

            if (!stats.Farms.ContainsKey(item.info.shortname))
            {
                stats.Farms.Add(item.info.shortname, 0);
            }

            stats.Farms[item.info.shortname] += item.amount;
        }

        #endregion

        #region Commands

        [ChatCommand("x")]
        private void sad(BasePlayer player)
        {
            OnPlayerDisconnected(player);
        }

        private static string Layer = "UI_VKBot";
        private void AuthInterface(BasePlayer player, string command, string[] args)
        {
            CuiHelper.DestroyUi(player, Layer);
            CuiElementContainer container = new CuiElementContainer();

            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin = "0 0", OffsetMax = "0 0"},
                Image         = {Color     = "0 0 0 0"}
            }, "Overlay", Layer);
            
            container.Add(new CuiButton
            {
                RectTransform = { AnchorMin = "0 0", AnchorMax = "1 1", OffsetMax = "0 0" },
                Button = { Color = "0 0 0 0", Close = Layer },
                Text = { Text = "" }
            }, Layer);
            
            container.Add(new CuiPanel
            {
                RectTransform = { AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = "-300 -50", OffsetMax = "300 50" },
                Image = { Color = "1 1 1 0.3" }
            }, Layer);

            CuiHelper.AddUi(player, container);
        }

        #endregion

        #region Interface

        

        #endregion
    }
}